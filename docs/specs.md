
#Controle de Estoque


> Eu como comprador
> 
> quero adicionar produtos comprados ao estoque
> 
> para manter o saldo em estoque atualizado;

> Eu como almoxarifado,
> 
> Quero visualizar o estoque de produtos
> 
> Para efetuar a compra de reposição de um produto;

> Eu como almoxarifado,
> 
> devo dar baixa de uma quantidade de produto 
> 
> para manter o saldo em estoque atualizado;

> Eu como financeiro
> 
> quero ser notificado de entradas e saidas em estoque
> 
> para emitir relatórios de controle
