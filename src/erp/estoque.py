from util.pub_sub import Event


class Produto(object):

    def __init__(self, id):
        self.id = id


class Estoque(Event):

    def __init__(self):
        self.estoque = []
        super(Estoque, self).__init__()

    def adicionar(self, produto, quantidade):
        movimento = {
            'produto': produto,
            'quantidade': quantidade
        }

        self.estoque.append(movimento)
        self.emit('produto-adicionado', movimento)

    def baixar(self, produto, quantidade):
        nao_ha_saldo = not (self.saldo_por_produto(produto) - quantidade > 0)
        if nao_ha_saldo:
            raise Exception()

        movimento = {
            'produto': produto,
            'quantidade': quantidade * -1
        }

        self.estoque.append(movimento)
        self.emit('produto-baixado', movimento)

    def saldo_por_produto(self, produto):
        saldo = 0

        for item in self.estoque:
            if item.get('produto').id == produto.id:
                saldo += item.get('quantidade')

        return saldo
