import unittest
from unittest.mock import Mock
from erp.estoque import Produto, Estoque


class TestEstoque(unittest.TestCase):

    def test_deve_adicionar_produto_ao_estoque(self):
        # definições
        produto = Produto(id=1)
        estoque = Estoque()

        # ação
        estoque.adicionar(produto, quantidade=100)

        # asserção
        saldo_produto = estoque.saldo_por_produto(produto)
        self.assertTrue(saldo_produto==100)

    @unittest.expectedFailure
    def test_deve_falhar_ao_baixar_produto_sem_estoque(self):
        # definições
        produto = Produto(id=1)
        estoque = Estoque()

        # ação
        estoque.baixar(produto, quantidade=100)

    def test_deve_baixar_produto_do_estoque(self):
        # definições
        produto = Produto(id=1)
        estoque = Estoque()

        # ação
        estoque.adicionar(produto, quantidade=100)
        estoque.baixar(produto, quantidade=10)

        # asserção
        saldo_produto = estoque.saldo_por_produto(produto)
        self.assertTrue(saldo_produto==90)

    def test_deve_adicionar_varios_produtos_no_estoque(self):
        # definições
        bicicleta = Produto(id=1)
        camiseta = Produto(id=2)
        estoque = Estoque()

        # ação
        estoque.adicionar(bicicleta, quantidade=100)
        estoque.adicionar(camiseta, quantidade=58)

        # asserção
        saldo_bicicleta = estoque.saldo_por_produto(bicicleta)
        saldo_camiseta = estoque.saldo_por_produto(camiseta)
        self.assertTrue(saldo_bicicleta==100)
        self.assertTrue(saldo_camiseta==58)

    def test_deve_notificar_movimentacao_em_estoque(self):
        produto_adicionado = Mock()
        produto_baixado = Mock()

        # definições
        produto = Produto(id=1)
        estoque = Estoque()

        estoque.on('produto-adicionado', produto_adicionado)
        estoque.on('produto-baixado', produto_baixado)

        # ação
        estoque.adicionar(produto, quantidade=100)
        estoque.baixar(produto, quantidade=10)

        # asserção
        saldo_produto = estoque.saldo_por_produto(produto)
        self.assertTrue(saldo_produto==90)

        self.assertTrue(produto_adicionado.called)
        self.assertTrue(produto_baixado.called)
